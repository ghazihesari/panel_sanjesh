<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Auth\User;

class checkAdminAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            if(auth()->user()->level==1)
            {
               // return view('Admin.panel');
                return $next($request);
            }
        if(auth()->user()->level==0)
        {
             return redirect('/user/panel');
          //  return $next($request);
        }

        return redirect('/');
    }


}
