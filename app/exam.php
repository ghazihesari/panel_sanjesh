<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class exam extends Model
{
    protected $fillable = [
        'id','title', 'description', 'true_response',

    ];

    public function interface_exam_user()
    {
        return $this->hasMany(interface_exam_user::class);
    }
}
