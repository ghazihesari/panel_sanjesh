<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class interface_exam_user extends Model
{
    protected $fillable = [
        'id','user_id', 'exam_id', 'user_response',

    ];


    public function user()
    {
        return $this->belongsTo(user::class);
    }
    public function exam()
    {
        return $this->belongsTo(exam::class);
    }
}
