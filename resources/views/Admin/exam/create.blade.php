@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>وارد کردن سوالات امتحانی</h2>
        </div>
        <form class="form-horizontal" action="{{ route('exam.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">موضوع </label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="موضوع خود را وارد نمایید" value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="soal" class="control-label">متن سوال </label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="متن سوال خود را وارد نمایید" value="{{ old('description') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="true_response" class="control-label">جواب صحیح </label>
                    <input type="text" class="form-control" name="true_response" id="true_response" placeholder="جواب صحیح خود را وارد نمایید" value="{{ old('true_response') }}">
                </div>
            </div>










            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger">ارسال</button>
                </div>
            </div>
        </form>
    </div>
@endsection