<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email', 'password',
        'level','user_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function interface_exam_user()
    {
        return $this->hasMany(interface_exam_user::class);
    }

   /* public function isAdmin()
    {
        return $this->level == 'admin' ? true : false;
    }
    public function isUser()
    {
        return $this->level == 'user' ? true : false;
    }*/
}
