@extends('Admin.master')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>لیست سوالات</h2>
            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>موضوع</th>
                    <th>سوال امتحانی</th>
                    <th>پاسخ صحیح</th>
                    <th>تاریخ</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>

                @foreach($values as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->title }}</td>
                        <td>{{ $value->description }}</td>
                        <td>{{ $value->true_response }}</td>
                        <td>{{ $value->created_at }}</td>


                        <td>
                            <form action="{{ route('exam.destroy'  , ['id' => $value->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach




                </tbody>
            </table>
        </div>
        <div style="text-align: center">

        </div>
    </div>
@endsection