<?php

namespace App\Http\Controllers;

use App\interface_exam_user;
use Illuminate\Http\Request;

class InterfaceExamUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\interface_exam_user  $interface_exam_user
     * @return \Illuminate\Http\Response
     */
    public function show(interface_exam_user $interface_exam_user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\interface_exam_user  $interface_exam_user
     * @return \Illuminate\Http\Response
     */
    public function edit(interface_exam_user $interface_exam_user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\interface_exam_user  $interface_exam_user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, interface_exam_user $interface_exam_user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\interface_exam_user  $interface_exam_user
     * @return \Illuminate\Http\Response
     */
    public function destroy(interface_exam_user $interface_exam_user)
    {
        //
    }
}
