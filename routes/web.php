<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return redirect('/admin/panel');
});


Auth::routes();

Route::get('/home', 'HomeController@index');


$this->get('/admin/panel', 'Admin\PanelController@adminindex')->middleware('checkAdmin');
$this->get('/user/panel', 'Admin\PanelController@userindex');

Route::group(['namespace' => 'Admin' ,  'middleware' => ['auth','checkAdmin'], 'prefix' => 'admin'],function () {
    $this->get('/panel', 'PanelController@adminindex');
    $this->resource('/panel/exam', 'ExamController');

});

Route::group([ 'middleware' => ['auth','checkAdmin'], 'prefix' => 'user'],function () {
    //$this->get('/panel', 'Admin\PanelController@userindex');

});



Route::group(['namespace' => 'Auth'] , function (){
    // Authentication Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('login');
    $this->post('login', 'LoginController@login');
    $this->get('logout', 'LoginController@logout')->name('logout');

    // Login And Register With Google
   // $this->get('login/google', 'LoginController@redirectToProvider');
    // $this->get('login/google/callback', 'LoginController@handleProviderCallback');
    // Registration Routes...
    $this->get('register', 'RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset');
});
