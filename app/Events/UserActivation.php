<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserActivation implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    use Notifiable;

    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user=$user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('sendmsg.user');

      //  return new PrivateChannel('user.' . $this->user->id);
    }
   /* public function broadcastWith()
    {
        return ['id' => $this->user->id];
    }

    public  function BroadcastToCurrentUser()
    {
        return [
            'message'=>'sallam va doorod'

        ];
    }*/

}
